import { RobolejosBrowserPage } from './app.po';

describe('robolejos-browser App', () => {
  let page: RobolejosBrowserPage;

  beforeEach(() => {
    page = new RobolejosBrowserPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
