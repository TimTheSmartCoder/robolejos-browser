import {Subject} from "./subject";
/**
 * Created by Morten on 03-04-2017.
 */
export class Category {
  uid: string;
  title: string;
  description: string;
  subjects?: Subject[];
}
