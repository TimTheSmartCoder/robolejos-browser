export class Profile {
  public email: string;
  public username: string;
}

export class User {
  public profile: Profile;
  public uid: string;
  /**
   * Creates an empty user object.
   * @returns {User}
   * @constructor
   */
  public static Create(): User {
    const user: User = new User();
    user.profile = new Profile();
    return user;
  }


}
