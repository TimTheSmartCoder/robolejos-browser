/**
 * Created by Mcbeer on 24-04-2017.
 */
export class Subject {
  categoryUid: string;
  title: string;
  uid: string;
  description: string;
  imageUrl: string;
}
