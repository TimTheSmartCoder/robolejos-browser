
/**
 * Created by Mcbeer on 24-04-2017.
 */
export class Post {
  info: Info;
  uid: string;


  constructor() {
    this.info = new Info();
  }
}

export class Info{
  title: string;
  content: string;
  views: number;
  subjectUid: string;
  userUid: string;
  image: string;
  username: string;
  images: string[];
}

