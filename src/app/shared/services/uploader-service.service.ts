import { Injectable, Inject } from '@angular/core';
import { AngularFire, FirebaseApp } from 'angularfire2';
import { User } from 'app/shared/models/user';
import { ReplaySubject } from 'rxjs/ReplaySubject';

@Injectable()
export class UploaderServiceService {

  constructor(public af: AngularFire, @Inject(FirebaseApp) private firebaseApp: any) { }

  /**
   * Uploads the profile picture to Firebase.
   * @param user
   * @param img
   */
  uploadAvatar(user, img) {
    const storageRef = this.firebaseApp.storage().ref();

    const af = this.af;
    const path = `/users/${user.uid}`;
    const iRef = storageRef.child(path);

    iRef.putString(img, 'base64', {contentType: 'image/png'}).then((snapshot) => {console.log('Uploaded something');

    af.database.object(`users/${user.uid}/profile/image`).update({path: path, filename: user.uid}); });
  }

  /**
   * Retuns the profile picture
   * @param user
   * @returns {ReplaySubject}
   */
  getProfileImage(user: User): ReplaySubject<any> {
    const resultSubject = new ReplaySubject();
    const storage = this.firebaseApp.storage();

    this.af.database.object(`users/${user.uid}/profile/image`)
    .subscribe(image => {
      console.log('image', image);
      if (image.path != null) {
        console.log('one', image);
        const pathReference = storage.ref(image.path);
        pathReference.getDownloadURL().then(url => {
          const result = {image: url, path: image.path, filename: image.filename};
          console.log('two', result);
          resultSubject.next(result);
        });
      }
    });
    return resultSubject;
  }

}
