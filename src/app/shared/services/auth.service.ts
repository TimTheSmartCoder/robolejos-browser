import { Injectable } from '@angular/core';
import { ReplaySubject} from 'rxjs/ReplaySubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import { FirebaseAuthState,
         AngularFire,
         AuthProviders,
         AuthMethods as AngularFireAuthMethods,
} from 'angularfire2';
import {User} from '../models/user';
import {UserService} from './user.service';
import auth = firebase.auth;

/**
 * Enum for Authentication methods avaible.
 */
export enum AuthMethods {
  Email,
  Twitter,
  Facebook,
  Google
}

/**
 * Different Authentication errors.
 */
export class AuthErrors {

  private static readonly Errors = {
    'auth/invalid-email'                            : 'Invalid Email.',
    'auth/operation-not-allowed'                    : 'Operation not allowed.',
    'auth/user-disabled'                            : 'User is disabled.',
    'auth/user-not-found'                           : 'User do not exist.',
    'auth/wrong-password'                           : 'Wrong password.',
    'auth/account-exists-with-different-credential' : 'Account exists with different crendentials.',
    'auth/invalid-credentials'                      : 'Invalid credentials.',
    'auth/email-already-in-use'                     : 'Email already in use.',
    'weak-password'                                 : 'Weak password.'
  };

  public static GetMessage(error: string): string {
    if (AuthErrors.Errors.hasOwnProperty(error)) {
      return AuthErrors.Errors[error];
    } else {
      return 'Unkown error.';
    }
  }
}

/**
 * Authentication service for handling authentication
 * in the application.
 */
@Injectable()
export class AuthService {

  constructor(private angularFire: AngularFire, private userService: UserService) { }

  /**
   * Authenticate the user with the given Authentication method and data.
   * @param authMethod - Authentication method. Ex. Password.
   * @param remember - Remember the user.
   * @param data - Data for an authentication method.
   * @returns {Observable<boolean>}
   */
  public authenticate(authMethod: AuthMethods, remember: boolean , data?): Observable<boolean> {
    switch (authMethod) {
      case AuthMethods.Email:
        return this.internalAuthenticate(data,
          { provider: AuthProviders.Password, method: AngularFireAuthMethods.Password, remember: remember });
      case AuthMethods.Facebook:
        return this.internalAuthenticate(
          { provider: AuthProviders.Facebook, method: AngularFireAuthMethods.Popup, remember: remember });
      case AuthMethods.Twitter:
        return this.internalAuthenticate(
          { provider: AuthProviders.Twitter, method: AngularFireAuthMethods.Popup, remember: remember });
      case AuthMethods.Google:
        return this.internalAuthenticate(
          { provider: AuthProviders.Google, method: AngularFireAuthMethods.Popup, remember: remember });
    }
  }

  /**
   * Register the given user.
   * @param authMethod - Autentication method to use.
   * @param data - Extra data for E-mail registring.
   * @returns {Observable<boolean>}
   */
  public registerAuth(authMethod: AuthMethods , data?): Observable<boolean> {
    if (authMethod === AuthMethods.Email) {
      return Observable.from(this.angularFire.auth.createUser(data)).switchMap(
        authState => Observable.of(authState ? true : false));
    }
    return this.authenticate(authMethod, true, data);
  }

  /**
   * Register user information.
   * @param user - User to register.
   * @returns {Observable<boolean>}
   */
  public registerUser(user: User): Observable<boolean> {
    return this.angularFire.auth.switchMap(authState => {
      if (!authState) { return Observable.of(false); }
      user.uid = authState.auth.uid;
      user.profile.email = authState.auth.email;
      return this.userService.create(user).switchMap(state => {
        return Observable.of(state ? true : false);
      });
    });
  }

  /**
   * Checks if the user have authentication, but not have
   * registered any user information.
   * @returns {Observable<boolean>}
   */
  public onlyAuthenticated(): Observable<boolean> {
    return this.angularFire.auth.switchMap(authState => {
      if (!authState) { return Observable.of(false); }
      return this.userService.exists(authState.auth.uid).switchMap(state => {
        return Observable.of(state ? false : true); });
    });
  };

  /**
   * Returns true or false, accordinly to the users authentication
   * status.
   * @returns {Observable<R>}
   */
  public isAuthenticated(): Observable<boolean> {
    return this.onlyAuthenticated().switchMap(state => {
      return this.angularFire.auth.switchMap(authState => {
        return Observable.of(authState && !state ? true : false);
      });
    });
  }

  /**
   * Gets the current authenticated user.
   * @returns {ReplaySubject<User>}
   */
  public currentUser(): Observable<User> {
    return this.isAuthenticated().switchMap(state => {
      if (state) {
        return this.angularFire.auth.switchMap(authState => {
          return this.userService.get(authState.auth.uid);
        });
      }
      return Observable.of(null);
    });
  }

  /**
   * Logs out the user from the authentication.
   * @returns {any}
   */
  public logout(): Observable<void> {
    const promise = this.angularFire.auth.logout();
    return Observable.from(promise);
  }

  /**
   * Internal authentication method. This method is used by the public
   * method authenticate to process authentication request.
   * @param p1 - data object.
   * @param p2 - data object.
   * @returns {Observable<boolean>}
   */
  private internalAuthenticate(p1, p2?):  Observable<boolean> {
    // Connect to firebase and authenticate the user with email and password. After
    // return the AuthState.
    let promise;

    if (p2) {
      promise = <Promise<FirebaseAuthState>> this.angularFire.auth.login(p1, p2);
    } else {
      promise = <Promise<FirebaseAuthState>> this.angularFire.auth.login(p1);
    }
    // Convert the promise to an observable, after switch the observable
    // to point to the inner which return true or false.
    return Observable.from(promise).switchMap(state => {
      return Observable.of((state) ? true : false);
    });
  }
}
