import { Injectable } from '@angular/core';
import { AngularFire, FirebaseAuthState} from 'angularfire2';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { User } from '../models/user';
import { User as FirebaseUser } from 'firebase';
import {createLanguageService} from "tslint";

@Injectable()
export class UserService {

  constructor(private angularFire: AngularFire) { }

  /**
   * Creates the given user.
   * @param user - User to create.
   * @returns {ReplaySubject<User>}
   */
  public create(user: User): ReplaySubject<User> {
    const replaySubject: ReplaySubject<User> = new ReplaySubject<User>();
    const data = { };
    data[`users/${user.uid}`] = user;
    this.angularFire.database.object('').update(data)
      .then(() => { replaySubject.next(user); })
      .catch(error => { replaySubject.error(error); });
    return replaySubject;
  }

  public exists(uid): Observable<boolean> {
    return this.angularFire.database.object(`users/${uid}`).switchMap(user => {
      return Observable.of(user.$exists());
    });
  };

  /**
   * Gets the user with the given uid.
   * @param uid - Uid of the user to get.
   * @returns {Observable<User>}
   */
  public get(uid: string): Observable<User> {
    return this.angularFire.database.object(`users/${uid}`);
  }


  public getAvatar(uid: string) {
    const storageRef = this.angularFire.database.object(`users/profile/image/${uid}`);
  }
}
