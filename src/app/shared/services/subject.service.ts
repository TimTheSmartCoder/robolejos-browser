import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from '../models/subject';
import {AngularFire} from 'angularfire2';
import {ReplaySubject} from 'rxjs/ReplaySubject';

@Injectable()
export class SubjectService {

  constructor(private angularFire: AngularFire) { }

  /**
   * Gets all subjects
   * @returns {FirebaseListObservable<any[]>}
   * @constructor
   */
  public GetAll(): Observable<Subject[]> {
    return this.angularFire.database.list('subjects');
  }

  /**
   * Retuns what you want, when you want.
   * @param uid
   * @returns {FirebaseObjectObservable<any>}
   * @constructor
   */
  public Get(uid: string): Observable<Subject> {
    return this.angularFire.database.object(`subjects/${uid}`);
  }

  /**
   * Retuns based on a category.
   * @param categoryUid
   * @returns {FirebaseListObservable<any[]>}
   * @constructor
   */
  public GetByCategory(categoryUid: string) : Observable<Subject[]> {
    return this.angularFire.database.list('subjects', {
      query: {
        orderByChild: 'categoryUid',
        equalTo: categoryUid
      }
    });
  }
}
