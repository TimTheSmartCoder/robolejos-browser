import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Reply} from '../models/reply';
import {AngularFire} from 'angularfire2';
import {ReplaySubject} from 'rxjs/ReplaySubject';

@Injectable()
export class ReplyService {

  constructor(private angularFire: AngularFire) { }

  public GetAll(postid: String): Observable<Reply[]> {
    return this.angularFire.database.list(`replies/${postid}`);
  }


  /**
   * Creates a reply, based on reply and postid.
   * @param reply
   * @param postid
   * @returns {ReplaySubject<Reply>}
   */
  public create(reply: Reply, postid: String): ReplaySubject<Reply> {
    const replaySubject: ReplaySubject<Reply> = new ReplaySubject<Reply>();
    this.angularFire.database.list(`replies/${postid}/`).push(reply).then(() => {replaySubject.next(reply);})
      .catch(error => {replaySubject.error(error);});
    return replaySubject;
  }

}
