import {Inject, Injectable} from '@angular/core';
import {AngularFire, FirebaseApp} from 'angularfire2';
import {Observable} from 'rxjs/Observable';
import {Post} from '../models/post';
import {ReplaySubject} from 'rxjs/ReplaySubject';

@Injectable()
export class PostService {


  constructor(private angularFire: AngularFire, @Inject(FirebaseApp) private firebaseApp: any) { }

  /**
   * Retunes it all!
   * @returns {FirebaseListObservable<any[]>}
   * @constructor
   */
  public GetAll(): Observable<Post[]> {
    return this.angularFire.database.list('posts');
  }

  /**
   *  Get a user, via uid
   * @param uid
   * @returns {ReplaySubject}
   * @constructor
   */
  public Get(uid: string): Observable<Post> {
    let replaySubject = new ReplaySubject();
    this.firebaseApp.database().ref(`posts/${uid}`).once('value').then(snapshot => {
      let post = snapshot.val();
      if (!post.uid) post.uid = snapshot.key;
      replaySubject.next(post);
    });
    return replaySubject;
  }

  /**
   *  Returnes a users posts.
   * @param userUid
   * @param count
   * @returns {FirebaseListObservable<any[]>}
   * @constructor
   */
  public GetByUserId(userUid: string, count: number): Observable<Post[]> {
    return this.angularFire.database.list('posts', {
      query: {
        orderByChild: 'info/userUid',
        equalTo: userUid,
        limitToLast: count
      }
    });
  }

  /**
   * Creates posts.
   * @param post
   * @returns {ReplaySubject<Post>}
   */
  public create(post: Post): ReplaySubject<Post> {
    const replaySubject: ReplaySubject<Post> = new ReplaySubject<Post>();
    this.angularFire.database.list('posts').push(post)
      .then((snapshot) => { post.uid = snapshot.key; replaySubject.next(post); })
      .catch(error => { replaySubject.error(error); });
    return replaySubject;
  }

  /**
   * Edits a post
   * @param title
   * @param content
   * @param uid
   * @returns {any}
   */
  public editPost(title: String, content: String, uid: String): Observable<void> {
    console.log("Service: " + title + "/" + content);
    return Observable.from(this.angularFire.database.object(`posts/${uid}/info`).update({
      title: title,
      content: content
    }));
  }

  /**
   * Uploads images
   * @param postUid
   * @param images
   * @returns {Observable<R|T>}
   */
  public uploadImages(postUid: string, images): Observable<string[]> {
    const storageRef = this.firebaseApp.storage().ref();
    const promises = [];

    for (const image of images){
      const path = `/posts/${postUid}/${image.name}`;
      const ref = storageRef.child(path);
      promises.push(ref.put(image, {name: image.name}));
    }

    return Observable.from(Promise.all(promises)).switchMap(snapshots => {
      const fileNames: any = [];
      for(let i = 0; i < images.length; i++) {
        fileNames.push(images[i].name);
      }
      console.log(fileNames);
      return Observable.of(fileNames);
    }).catch( error => {
      return Observable.of(null);
    });
  }

  /**
   * Uploads stuff
   * @param postUid
   * @param images
   * @returns {Observable<R>}
   */
  public upload(postUid: string, images): Observable<boolean> {
    return this.uploadImages(postUid, images).switchMap(state => {
      if (state) {
        let ob: any = {};
        let i = 0;
        for (let image of state) {
          console.log(image);
          ob[i] = image;
          i++;
        }
        return Observable.from(this.angularFire.database.object(`posts/${postUid}/info/images`).set(ob)).switchMap(state => {
          return Observable.of(true);
        });
      } else {
        return Observable.of(false);
      }
    });
  }

  /**
   * Deletes the given post object from the database and
   * it will also delete all related replies.
   * @param uid
   * @returns Observable.
   */
  public delete(uid: string): Observable<boolean> {
      const ref = this.firebaseApp.database().ref('/');
      const references = {};
      references[`posts/${uid}/`] = null;
      references[`replies/${uid}/`] = null;
      return Observable.from(ref.update(references));
  }

  /**
   * returns the 10 newest posts created.
   * @returns {FirebaseListObservable<any[]>}
   */
  public getNewestPosts(count: number): Observable<Post[]> {
    return this.angularFire.database.list('posts', {query: {limitToLast: count}});
  }

  /**
   * Returns newest post from subject.
   * @param subjectUid
   * @param count
   * @returns {FirebaseListObservable<any[]>}
   */
  public getNewestPostFromSubject(subjectUid: string, count: number): Observable<Post[]> {
    return this.angularFire.database.list('posts', {
      query: {
        orderByChild: 'info/subjectUid',
        equalTo: subjectUid,
        limitToLast: count
      }
    });
  }

  /**
   * Returns the images.
   * @param postUid
   * @returns {ReplaySubject}
   */
  public getImagesForPost(postUid: string) : Observable<string[]> {
    const replaySubject = new ReplaySubject();
    const ref = this.firebaseApp.storage().ref(`posts/${postUid}`);
    this.firebaseApp.database().ref(`posts/${postUid}/info/images`).once('value', function(snapshot) {
      if (snapshot.val() != null) {
        const images = [];
        const promises = [];
        for (const image of snapshot.val()) {
          promises.push(ref.child(image).getDownloadURL().then(url => {
            images.push(url);
          }));
        }
        Promise.all(promises).then(() => {
          replaySubject.next(images);
        });
      }
    });
    return replaySubject;
  }

  /**
   * Retuns posts based on subjectid
   * @param subjectUid
   * @returns {FirebaseListObservable<any[]>}
   */
  public getPostsBySubjectId(subjectUid: string): Observable<Post[]> {
    console.log(subjectUid);
    return this.angularFire.database.list('posts', {
      query: {
        orderByChild: 'info/subjectUid',
        equalTo: subjectUid
      }
    });
  }
}
