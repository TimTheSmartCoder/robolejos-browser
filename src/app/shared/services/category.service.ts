import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Category } from './../models/category';
import { AngularFire } from 'angularfire2';

@Injectable()
export class CategoryService {

  constructor(private angularFire: AngularFire) { }

  /**
   * Retuns a list of categories
   * @returns {FirebaseListObservable<any[]>}
   * @constructor
   */
  public GetAll(): Observable<Category[]> {
    return this.angularFire.database.list('categories');
  }

  /**
   * Returns a single category by uid
   * @param uid
   * @returns {FirebaseObjectObservable<any>}
   * @constructor
   */
  public Get(uid: string): Observable<Category> {
    return this.angularFire.database.object(`categories/${uid}`);
  }

}
