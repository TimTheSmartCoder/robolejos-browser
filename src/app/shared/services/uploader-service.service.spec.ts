import { TestBed, inject } from '@angular/core/testing';

import { UploaderServiceService } from './uploader-service.service';

describe('UploaderServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UploaderServiceService]
    });
  });

  it('should ...', inject([UploaderServiceService], (service: UploaderServiceService) => {
    expect(service).toBeTruthy();
  }));
});
