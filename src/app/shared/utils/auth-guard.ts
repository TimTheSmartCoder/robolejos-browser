import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.auth.isAuthenticated()
      .do(authenticated => {
        if (!authenticated) {
          this.auth.onlyAuthenticated().first().subscribe(state => {
            if (state) {
              this.router.navigate(['/register']);
            } else {
              this.router.navigate(['/login']);
            }
          });
        }
      });
  }
}
