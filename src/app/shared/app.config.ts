import {AuthProviders, AuthMethods} from 'angularfire2';

/**
 * Firebase configuration.
 * @type {{apiKey: string; authDomain: string; databaseURL: string; storageBucket: string; messagingSenderId: string}}
 */
export const firebaseConfig = {
  apiKey: 'AIzaSyDBTyEGuZGF1TpLCCQxJ2laGbrRE3cv9xA',
  authDomain: 'robolejos.firebaseapp.com',
  databaseURL: 'https://robolejos.firebaseio.com',
  projectId: 'robolejos',
  storageBucket: 'robolejos.appspot.com',
  messagingSenderId: '661669394346'
};

/**
 * Firebase authentication login config.
 * @type {{provider: AuthProviders; method: AuthMethods}}
 */
export const firebarebaseLoginConfig =  {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
};
