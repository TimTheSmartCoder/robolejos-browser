import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {AuthMethods} from '../../shared/services/auth.service';

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.css']
})
export class LoginViewComponent implements OnInit {

  /**
   * User model.
   */
  public user: { email: string, password: string, rememberMe: boolean };

  /**
   * Loading input for display a spinner, while
   * loading of authentication.
   */
  @Input('loading')
  public loading: boolean;

  /**
   * Error input for display an error message, from
   * the outside, possible from Smart component.
   */
  @Input('error')
  public error: string;

  /**
   * EventEmitter for callback.
   */
  @Output()
  public login: EventEmitter<{ auth: AuthMethods, model: object, rememberMe: boolean }>;


  constructor() {

    // Set the default user object.
    this.user = { email: '', password: '', rememberMe: false };
    this.login = new EventEmitter<{ auth: AuthMethods, model: object, rememberMe: boolean  }>();
  }

  ngOnInit() { }

  /**
   * OnSubmit method, to be called when the user submit login
   * form.
   */
  public onSubmit(e): void {
    // Prevent submit of the form.
    e.preventDefault();
    // Emit the event to the smart component listening.
    this.login.emit({ auth: AuthMethods.Email, model: this.user, rememberMe: this.user.rememberMe  });
  }

  public loginWithFacebook(): void {
    // Emit the event to the smart component listening.
    this.login.emit({ auth: AuthMethods.Facebook, model: this.user, rememberMe: this.user.rememberMe  });
  }
}
