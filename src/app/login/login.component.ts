import {Component, OnInit, OnDestroy} from '@angular/core';
import {AuthService, AuthMethods, AuthErrors} from '../shared/services/auth.service';
import { Subscription } from 'rxjs/Subscription';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  /**
   * Subscription for login.
   */
  private loginRequest: Subscription;

  /**
   * Error string for containing any errors on authentication.
   */
  private error: string;

  /**
   * Contains state if the login component already is
   * loading.
   */
  private loading = true;

  private isAuthenticatedSubscription: Subscription;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.isAuthenticatedSubscription = this.authService.isAuthenticated().subscribe(authState => {
      if (authState) {
        this.router.navigate(['/']);
      } else {
        this.authService.onlyAuthenticated().first().subscribe(state => {
          if (state) {
            this.router.navigate(['/register']);
          } else {
            this.loading = false;
          }
        });
      }
    });
  }

  ngOnDestroy() {

    // Unsubscribe from the auth service.
    if (this.loginRequest) {
      this.loginRequest.unsubscribe();
    }
    if (this.isAuthenticatedSubscription) {
      this.isAuthenticatedSubscription.unsubscribe();
    }
  }

  /**
   * Called when the user submits a valid logins
   * information.
   * @param user
   */
  public login(model): void {
    this.loading = true;
    // Try to authenticate with the auth service.
    this.loginRequest =
      this.authService.authenticate(model.auth, model.rememberMe, {email: model.model.email, password: model.model.password})
        .subscribe(state => {
          this.error = '';
      }, error => {
          this.error = AuthErrors.GetMessage(error.code);
          this.loading = false;
        })
    ;
  }
}
