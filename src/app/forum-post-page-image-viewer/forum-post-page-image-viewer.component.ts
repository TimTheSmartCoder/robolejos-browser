import {Component, OnInit, Input} from '@angular/core';
import {PostService} from "../shared/services/post.service";

@Component({
  selector: 'app-forum-post-page-image-viewer',
  templateUrl: './forum-post-page-image-viewer.component.html',
  styleUrls: ['./forum-post-page-image-viewer.component.css']
})
export class ForumPostPageImageViewerComponent implements OnInit {

  @Input()
    postUid: string;

  images: string[] = [];


  constructor(private postService : PostService) {

  }

  ngOnInit() {
    //Gets all images belonging to a post and puts them in the images array
    this.postService.getImagesForPost(this.postUid).first().subscribe(images => {
        this.images = images;
    });
  }

}
