import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumPostPageImageViewerComponent } from './forum-post-page-image-viewer.component';

describe('ForumPostPageImageViewerComponent', () => {
  let component: ForumPostPageImageViewerComponent;
  let fixture: ComponentFixture<ForumPostPageImageViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForumPostPageImageViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumPostPageImageViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
