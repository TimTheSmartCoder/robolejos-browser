import { Component, OnInit } from '@angular/core';
import { User } from '../shared/models/user';
import { AuthService } from '../shared/services/auth.service';
import { UserService } from '../shared/services/user.service';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import { UploaderServiceService } from 'app/shared/services/uploader-service.service';
import {PostService} from '../shared/services/post.service';
import {Post} from '../shared/models/post';
import {Router} from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  data: any;
  cropperSettings: CropperSettings;
  changingImage: boolean;
  user: User;
  posts: any;
  postsList: Post[] = [];
  allMyNumbers: number;
  constructor(private uploadService: UploaderServiceService,
              private authService: AuthService,
              private userService: UserService,
              private postService: PostService,
              private router: Router
              ) {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 128;
    this.cropperSettings.height = 128;
    this.cropperSettings.croppedWidth = 128;
    this.cropperSettings.croppedHeight = 128;
    this.cropperSettings.canvasWidth = 400;
    this.cropperSettings.canvasHeight = 300;

    this.data = {};




  }

  ngOnInit() {
    this.authService.currentUser().first().subscribe(authuser => {
      this.user = authuser;
      this.userService.get(this.user.uid).first().subscribe(user => {
        this.user = user;
        this.uploadService.getProfileImage(user).subscribe(image => {this.data = image; });
        this.getMyPosts(this.postService, user);
        this.getByTheNumbers(this.postService, user);
      });
    });
  }

  /**
   * Boolean value to change.
   * Changes if you are changing image
   */
  changeImageClick() {
    this.changingImage = true;
  }

  /**
   * Runs uploadservces uploadAvatar, and sets boolean value to false;
   */
  saveNewImage() {
    if (this.data && this.data.image) {
      this.uploadService.uploadAvatar(this.user, this.data.image.split(/,(.+)/)[1]);
    }
    this.changingImage = false;
  }

  /**
   * Returns the users own posts.
   * Max of 20, to not fill up the box on profile page too much
   * @param postService
   * @param user
   */
  getMyPosts(postService: PostService, user: User) {
    this.posts = postService.GetByUserId(user.uid, 20);
  }

  /**
   * Returns amount of posts made, max of 1000
   * @param postService
   * @param user
   */
  getByTheNumbers (postService: PostService, user: User) {
    postService.GetByUserId(user.uid, 1000).first().subscribe(list => {
      this.postsList = list;
      this.allMyNumbers = this.postsList.length;
    });
  }

  /**
   * Calls postservice to delete post with uid
   * @param uid
   */
  deletePost(uid: string) {
    this.postService.delete(uid);
  }

  /**
   * Calls postservice to edit post with uid
   * Routes to the post in question
   * @param uid
   */
  editPost(uid: string) {
    this.router.navigate([`profile/${uid}/editpost`]);
  }

}
