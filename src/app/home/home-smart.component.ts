import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PostService} from '../shared/services/post.service';
import {Post} from '../shared/models/post';

@Component({
  selector: 'app-home-smart',
  templateUrl: './home-smart.component.html',
  styleUrls: ['./home-smart.component.css']
})
export class HomeSmartComponent implements OnInit {
  posts: Post[] = [];
  postSelesctedEvent: EventEmitter<String>;

  constructor(private postserive: PostService) {
  }

  /**
   * Gets the 5 newest post
   * via postservice get newestposts.
   * gets a list on succesful subscription.
   */
  ngOnInit() {
    this.postserive.getNewestPosts(5).first().subscribe(list => {
      this.posts = list;
    });
    console.log("postssize" + this.posts.length);
  this.postSelesctedEvent = new EventEmitter();
  }

}
