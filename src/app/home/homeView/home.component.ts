import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Post} from '../../shared/models/post';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @Input()
  posts: Post[];

  constructor() {
  }


  ngOnInit() {
  }

}
