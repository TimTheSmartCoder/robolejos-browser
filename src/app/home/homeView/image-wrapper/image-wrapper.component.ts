import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Post} from '../../../shared/models/post';
import {Router} from "@angular/router";



@Component({
  selector: 'app-image-wrapper',
  templateUrl: './image-wrapper.component.html',
  styleUrls: ['./image-wrapper.component.css']
})
export class ImageWrapperComponent implements OnInit {

  @Input()
  post: Post;



  constructor(private router: Router) { }

  ngOnInit() {

  }

  /**
   * navigates to the selected post.
   */
  public postSelected(): void{
    this.router.navigate([`/forum/subject/${this.post.info.subjectUid}/post/${this.post.uid}`]);
  }

}
