import { Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import { User } from '../../shared/models/user';
import { AuthMethods } from '../../shared/services/auth.service';
import {ImageCropperComponent, CropperSettings} from 'ng2-img-cropper';

@Component({
  selector: 'app-register-view',
  templateUrl: './register-view.component.html',
  styleUrls: ['./register-view.component.css']
})
export class RegisterViewComponent implements OnInit {

    data: any;
    cropperSettings: CropperSettings;
    changingImage: boolean;

  /**
   * Model of the registration.
   */
  private model: { user: User, password: string, repeatPassword: string };

  @Output()
  public registerAuthentication: EventEmitter<{ auth: AuthMethods, model: object }>;

  @Output()
  public registerInformation: EventEmitter<{ user: User, password: string}>;


  @Input()
  public auth = false;

  @Input()
  public info = false;

  /**
   * Error to show.
   */
  @Input()
  public error: string;

  constructor() {

    // Initialize model.
    this.model = { user: User.Create(), password: '', repeatPassword: '' };
    this.registerAuthentication = new EventEmitter<{ auth: AuthMethods, model: object }>();
    this.registerInformation = new EventEmitter<{ user: User, password: string}>();

    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 128;
    this.cropperSettings.height = 128;
    this.cropperSettings.croppedWidth =128;
    this.cropperSettings.croppedHeight = 128;
    this.cropperSettings.canvasWidth = 400;
    this.cropperSettings.canvasHeight = 300;
  }


  ngOnInit() { }

  /**
   * Called on submit of the form.
   * @param e
   */
  public onAuthSubmit(e) {
    e.preventDefault();
    this.registerAuthentication.emit(
      {auth: AuthMethods.Email, model: { email: this.model.user.profile.email, password: this.model.password }});
  }

  public onInfoSubmit(e): void {
    e.preventDefault();
    this.registerInformation.emit(this.model);
  }

  public signUpFacebook(): void {
    this.registerAuthentication.emit({auth: AuthMethods.Facebook, model: null});
  }
}
