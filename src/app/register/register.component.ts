import {Component, OnInit, OnDestroy} from '@angular/core';
import { UserService } from '../shared/services/user.service';
import { AuthErrors, AuthService } from '../shared/services/auth.service';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {

  /**
   * String to list error in and it will be
   * transfered to dum component.
   */
  public error: string;

  public auth = false;

  public info = false;

  private isAuthenticatedSubscription: Subscription;

  constructor(private userService: UserService, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.isAuthenticatedSubscription = this.authService.isAuthenticated().first().subscribe(authState => {
      if (authState) {
        this.router.navigate(['/']);
      } else {
        this.authService.onlyAuthenticated().first().subscribe(state => {
          if (state) {
            this.auth = false;
            this.info = true;
          } else {
            this.auth = true;
            this.info = false;
          }
        });
      }
    });
  }

  ngOnDestroy() {
    if (this.isAuthenticatedSubscription) {
      this.isAuthenticatedSubscription.unsubscribe();
    }
  }

  /**
   * Called by dump component on user creation.
   */
  public registerAuthentication(model): void {
    this.auth = false;
    this.authService.registerAuth(model.auth, model.model).first().subscribe(state => {
      console.log("logging in");
      if (state) {
        this.auth = false;
        this.info = true;
      }
    }, error => {
      this.auth = true;
      this.error = AuthErrors.GetMessage(error.code);
    });
  }

  public registerInformation(model): void {
    this.authService.registerUser(model.user).first().subscribe(state => {
      // Auth success.
      this.router.navigate(['/profile']);
    }, error => {
      this.info = true;
      this.error = AuthErrors.GetMessage(error.code);
    });
  }
}
