import { Component } from '@angular/core';
import {CategoryService} from './shared/services/category.service';
import {SubjectService} from './shared/services/subject.service';
import {PostService} from './shared/services/post.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';

  public constructor() {
  }
}

