// Angular official modules.
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularFireModule } from 'angularfire2';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Thridparty modules.
import 'hammerjs';
import {ImageCropperComponent, CropperSettings} from 'ng2-img-cropper';

// Services.
import { AuthService } from './shared/services/auth.service';
import {CategoryService} from './shared/services/category.service';
import {SubjectService} from './shared/services/subject.service';
import {PostService} from './shared/services/post.service';
import { UploaderServiceService } from "app/shared/services/uploader-service.service";

// Components.
import { AppComponent } from './app.component';
import { ForumComponent } from './forum/forum.component';
import { ForumCategoriesComponent } from './forum/forum-categories/forum-categories.component';

import { HomeComponent } from './home/homeView/home.component';
import { TopMenuComponent } from './top-menu/top-menu.component';
import { ImageWrapperComponent } from './home/homeView/image-wrapper/image-wrapper.component';

import { ProfileComponent } from './profile/profile.component';
import { ProfileViewComponent } from './profile/profile-view/profile-view.component';
import { RouterModule, Routes } from '@angular/router';

// Config.
import { firebaseConfig, firebarebaseLoginConfig } from './shared/app.config';
import { LoginComponent } from './login/login.component';
import { LoginViewComponent } from './login/login-view/login-view.component';
import { RegisterComponent } from './register/register.component';
import { RegisterViewComponent } from './register/register-view/register-view.component';
import { EqualDirective } from './shared/directives/equal.directive';
import {UserService} from './shared/services/user.service';
import {AppRoutingModule} from './app-routing.module';
import {AuthGuard} from './shared/utils/auth-guard';

import { ForumCategoryComponent } from './forum/forum-category/forum-category.component';
import { ForumSubjectComponent } from './forum/forum-subject/forum-subject.component';
import { ForumPostComponent } from './forum/forum-post/forum-post.component';
import { ForumPostListComponent } from './forum/forum-post-list/forum-post-list.component';
import { ForumPostListViewComponent } from './forum/forum-post-list/forum-post-list-view/forum-post-list-view.component';
import { ForumPostPageComponent } from './forum/forum-post-page/forum-post-page.component';
import { PostpageComponent } from './forum/postcreatesmart/postcreate/postcreate.component';
import { PostpagesmartComponent } from './forum/postcreatesmart/postcreatesmart.component';
import { OploaderComponent } from './forum/oploader/oploader.component';
import { ReplycreatesmartComponent } from './forum/replycreatesmart/replycreatesmart.component';
import { ReplyviewComponent } from './forum/replycreatesmart/replyview/replyview.component';
import { HomeSmartComponent } from './home/home-smart.component';
import {ReplyService} from "app/shared/services/reply.service";
import { ForumPostPageImageViewerComponent } from './forum-post-page-image-viewer/forum-post-page-image-viewer.component';
import { ForumPostEditComponent } from './forum/forum-post-edit/forum-post-edit.component';
import { PostEditViewComponent } from './forum/forum-post-edit/post-edit-view/post-edit-view.component';
import { ReplyListComponent } from './reply-list/reply-list.component';


@NgModule({
  declarations: [
    AppComponent,
    ForumComponent,
    ForumCategoriesComponent,
    HomeComponent,
    TopMenuComponent,
    ImageWrapperComponent,
    ProfileComponent,
    ProfileViewComponent,
    LoginComponent,
    LoginViewComponent,
    RegisterComponent,
    RegisterViewComponent,
    EqualDirective,
    ForumCategoryComponent,
    ForumSubjectComponent,
    ForumPostComponent,
    ForumPostListComponent,
    ForumPostListViewComponent,
    ForumPostPageComponent,
    PostpageComponent,
    PostpagesmartComponent,
    OploaderComponent,
    ImageCropperComponent,
    ReplycreatesmartComponent,
    ReplyviewComponent,
    HomeSmartComponent,
    ForumPostPageImageViewerComponent,
    ForumPostEditComponent,
    PostEditViewComponent,
    ReplyListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(firebaseConfig, firebarebaseLoginConfig),
    AppRoutingModule
  ],
  providers: [
    AuthService,
    UserService,
    AuthGuard,
    CategoryService,
    SubjectService,
    PostService,
    UploaderServiceService,
    ReplyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
