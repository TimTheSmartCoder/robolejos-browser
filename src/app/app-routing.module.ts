import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './shared/utils/auth-guard';
import { AppComponent } from './app.component';
import { ProfileComponent } from './profile/profile.component';
import { HomeComponent } from './home/homeView/home.component';
import { ForumComponent } from './forum/forum.component';
import {ForumPostListViewComponent} from "./forum/forum-post-list/forum-post-list-view/forum-post-list-view.component";
import {ForumPostPageComponent} from "./forum/forum-post-page/forum-post-page.component";
import {ReplycreatesmartComponent} from "./forum/replycreatesmart/replycreatesmart.component";
import {PostpageComponent} from './forum/postcreatesmart/postcreate/postcreate.component';
import {PostpagesmartComponent} from './forum/postcreatesmart/postcreatesmart.component';
import {HomeSmartComponent} from './home/home-smart.component';
import {ForumPostEditComponent} from "./forum/forum-post-edit/forum-post-edit.component";


const appRoutes = [
  { path: '', component: HomeSmartComponent},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', canActivate: [AuthGuard], component: ProfileComponent },
  { path: 'home', component: HomeSmartComponent },
  { path: 'forum', canActivate: [AuthGuard], component: ForumComponent },
  { path: 'forum/subject/:subid', canActivate: [AuthGuard], component: ForumPostListViewComponent},
  { path: 'forum/subject/:subid/post/:postid', canActivate: [AuthGuard], component: ForumPostPageComponent},
  { path: 'forum/subject/:subid', canActivate: [AuthGuard], children: [{
      path: 'create', component: PostpagesmartComponent
    }]
  },
  {path: 'reply', canActivate: [AuthGuard], component: ReplycreatesmartComponent},
  {path: 'profile/:postid/editpost', canActivate: [AuthGuard], component: ForumPostEditComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
