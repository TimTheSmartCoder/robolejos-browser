import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {Reply} from "../shared/models/reply";
import {ReplyService} from "../shared/services/reply.service";

@Component({
  selector: 'app-reply-list',
  templateUrl: './reply-list.component.html',
  styleUrls: ['./reply-list.component.css']
})
export class ReplyListComponent implements OnInit, OnDestroy {

  @Input()
  postUid: string;

  replies: Reply[];

  sub: any;

  constructor(private replyService: ReplyService) {

  }

  ngOnInit() {
  //Gets the observable list of replies for a post. This allows for live updating in the web app
   this.sub = this.replyService.GetAll(this.postUid);
  }

  ngOnDestroy() {

  }
}
