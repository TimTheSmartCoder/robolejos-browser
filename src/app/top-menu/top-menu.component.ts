import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../shared/services/auth.service';
import {Subscription} from 'rxjs/Subscription';
import {Router} from '@angular/router';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.css']
})
export class TopMenuComponent implements OnInit, OnDestroy {

  @Input()
  title: string;

  isAuthenticated = false;
  authenticationSub: Subscription;


  constructor(private authService: AuthService, private router: Router) { }

  /**
   * Gets authendications state via authService.
   */
  ngOnInit() {
    this.authenticationSub = this.authService.isAuthenticated().subscribe(state => {
      this.isAuthenticated = state;
    });
  }

  ngOnDestroy() {
    if (this.authenticationSub) {
      this.authenticationSub.unsubscribe();
    }
  }

  /**
   * Logs the user out via the authserive.
   */
  public logoutUser(): void {
    this.authService.logout().first().subscribe(() => {
      this.isAuthenticated = false;
    });
  }


}
