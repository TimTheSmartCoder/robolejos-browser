import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OploaderComponent } from './oploader.component';

describe('OploaderComponent', () => {
  let component: OploaderComponent;
  let fixture: ComponentFixture<OploaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OploaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
