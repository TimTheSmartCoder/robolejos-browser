import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';


@Component({
  selector: 'app-oploader',
  templateUrl: './oploader.component.html',
  styleUrls: ['./oploader.component.css']
})
export class OploaderComponent implements OnInit {
  @Output()
  public filesSelected: EventEmitter<any>;
  public files: Array<any>;

  constructor() {
    this.files = new Array<any>();
    this.filesSelected = new EventEmitter<any>();
  }

  ngOnInit() {
  }

  /**
   * Pushes the image to files and emits files selected event.
   * @param fileList
   */
  public changed(fileList){
    for (let i = 0; i < fileList.length; i++) {
      this.files.push(fileList[i]);
    }
    this.filesSelected.emit(this.files);
  }

}
