import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplycreatesmartComponent } from './replycreatesmart.component';

describe('ReplycreatesmartComponent', () => {
  let component: ReplycreatesmartComponent;
  let fixture: ComponentFixture<ReplycreatesmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplycreatesmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplycreatesmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
