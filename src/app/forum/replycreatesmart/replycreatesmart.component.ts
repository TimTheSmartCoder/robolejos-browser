import {Component, Input, OnInit, Output} from '@angular/core';
import {Reply} from "../../shared/models/reply";
import {UserService} from "../../shared/services/user.service";
import {User} from "../../shared/models/user";
import {ReplyService} from "../../shared/services/reply.service";
import {AuthService} from "../../shared/services/auth.service";

@Component({
  selector: 'app-replycreatesmart',
  templateUrl: './replycreatesmart.component.html',
  styleUrls: ['./replycreatesmart.component.css']
})
export class ReplycreatesmartComponent implements OnInit {


  @Input()
  postuid: String;

  @Output()
  replyCreated: boolean;

  constructor(private replyservice: ReplyService, private authservice: AuthService) {
  }

  ngOnInit() {
    this.replyCreated = false;
  }

  /**
   * Creates a reply via services.
   * Subcribes to userservice to fetch user information, adds it to reply object.
   * On subribe succes replyservice create is called and the reply object is sendt.
   * @param data
   */
  public createReply(data: any): void {
    this.replyCreated = false;
    this.authservice.currentUser().first().subscribe(user => {
      data.reply.info.username = user.profile.username;
      data.reply.info.userid = user.uid;
      this.replyservice.create(data.reply, this.postuid).first().subscribe(reply => {
        console.log(data);
        this.replyCreated = true;
      })
    });
  }
}
