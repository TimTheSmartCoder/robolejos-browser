import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Reply} from "../../../shared/models/reply";


@Component({
  selector: 'app-replyview',
  templateUrl: './replyview.component.html',
  styleUrls: ['./replyview.component.css']
})
export class ReplyviewComponent implements OnInit {

  reply: Reply;

  @Output()
  public createRelply: EventEmitter<any>;

  constructor() {
    this.reply = new Reply;
    this.createRelply = new EventEmitter();
  }

  onSubmit(e): void{
    e.preventDefault;
    this.createRelply.emit({reply: this.reply})
    this.reply = new Reply();
  }
  ngOnInit() {
  }

}
