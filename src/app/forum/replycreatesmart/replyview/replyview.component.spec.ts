import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplyviewComponent } from './replyview.component';

describe('ReplyviewComponent', () => {
  let component: ReplyviewComponent;
  let fixture: ComponentFixture<ReplyviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplyviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplyviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
