import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Post} from "../../../shared/models/post";
import {PostService} from "../../../shared/services/post.service";
import {Location} from "@angular/common";

@Component({
  selector: 'app-post-edit-view',
  templateUrl: './post-edit-view.component.html',
  styleUrls: ['./post-edit-view.component.css']
})
export class PostEditViewComponent implements OnInit {

  @Output()
  public editpost: EventEmitter<any>;

  @Input()
  title: string;
  @Input()
  content: string;

  constructor(private postservice: PostService, private location: Location) {
    this.editpost = new EventEmitter<any>();
  }

  ngOnInit() {
  }

  cancel(): void {
    this.location.back();
  }

  onSubmit(e): void {
    e.preventDefault();

    this.editpost.emit({title: this.title, content: this.content});
  }

}
