import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Post} from "../../shared/models/post";
import {PostService} from "../../shared/services/post.service";
import {Location} from "@angular/common";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-forum-post-edit',
  templateUrl: './forum-post-edit.component.html',
  styleUrls: ['./forum-post-edit.component.css']
})
export class ForumPostEditComponent implements OnInit {



  uid: string;
  title: string;
  content: string;
  post: Post;

  constructor(private postService: PostService, private loacation: Location, private route: ActivatedRoute) {
  }

  /**
   * gets the post to edit via the post serive with the potid.
   */
  ngOnInit() {
    this.route.params.first().subscribe(params => {
      this.uid = params['postid'];
      this.postService.Get(this.uid).first().subscribe(post => {
        this.post = post;
        this.title = this.post.info.title;
        this.content = this.post.info.content;
        console.log(this.title, this.content);
      })
    });
  }

  /**
   * Edits the post with the new data from the dum component
   * @param data
   */
  editPost(data : any): void {
    console.log("edit smat: " + data.title + " " +  data.content);
    this.postService.editPost(data.title, data.content, this.post.uid).first().subscribe(() => {
      this.loacation.back();
    });
  }
}
