import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumPostListViewComponent } from './forum-post-list-view.component';

describe('ForumPostListViewComponent', () => {
  let component: ForumPostListViewComponent;
  let fixture: ComponentFixture<ForumPostListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForumPostListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumPostListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
