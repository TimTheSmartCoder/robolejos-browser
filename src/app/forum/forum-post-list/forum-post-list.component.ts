import { Component, OnInit } from '@angular/core';
import {Post} from "../../shared/models/post";
import {ActivatedRoute} from "@angular/router";
import {Subject} from "../../shared/models/subject";
import {PostService} from "../../shared/services/post.service";
import {SubjectService} from "../../shared/services/subject.service";

@Component({
  selector: 'app-forum-post-list',
  templateUrl: './forum-post-list.component.html',
  styleUrls: ['./forum-post-list.component.css']
})
export class ForumPostListComponent implements OnInit {

  subject: Subject;

  id: string;
  private sub: any;

  posts: Post[] = []

  constructor(private postService: PostService, private subService: SubjectService , private route: ActivatedRoute) {

  }

  ngOnInit() {
    //Gets the subjectid from the URL by getting the params defined in the routing module
    this.sub = this.route.params.subscribe(params => {
      this.id = params['subid'];
    });
    //Gets the subject with the acquired subjectid
    this.subService.Get(this.id).first().subscribe(sub => {
      this.subject = sub;
    });
    //Gets all posts that contain the acquired subjectid.
    this.postService.getPostsBySubjectId(this.id).first().subscribe(list => {
      this.posts = list;
    });

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
