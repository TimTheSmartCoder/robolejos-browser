import {Component, Inject, OnInit} from '@angular/core';
import {PostService} from 'app/shared/services/post.service';
import {AuthService} from '../../shared/services/auth.service';
import {Post} from '../../shared/models/post';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularFire, FirebaseApp} from "angularfire2";
import {inject} from "@angular/core/testing";

@Component({
  selector: 'app-postpagesmart',
  templateUrl: './postcreatesmart.component.html',
  styleUrls: ['./postcreatesmart.component.css']
})
export class PostpagesmartComponent implements OnInit {

  private post: Post;
  private subjectId: string;
  public uploading = false;


  constructor(
    private authService: AuthService,
    private postService: PostService,
    private route: ActivatedRoute,
    private router: Router,
    @Inject(FirebaseApp) private firebaseApp: any) {
  }

  /**
   * Gets the subjectsid from the router parameters.
   */
  ngOnInit() {
    this.route.params.first().subscribe(params => {
      this.subjectId = params['subid'];
    });
  }

  /**
   * Composes and send the post to postservice.
   * postservice create creates the object. on subcribe succes
   * uploads the pictures via postservice upload.
   * @param data
   */
  public createPost(data: any): void {
    this.uploading = true;
    this.authService.currentUser().first().subscribe(user => {
      this.post = new Post();
      //Transfers information from the info object in data to the posts info object
      this.post.info.title = data.info.title;
      this.post.info.content = data.info.content;
      this.post.info.userUid = user.uid;
      this.post.info.username = user.profile.username;
      this.post.info.subjectUid = this.subjectId;
      this.post.info.images = data.images;

      this.postService.create(this.post).first().subscribe(post => {
        this.postService.upload(post.uid, this.post.info.images).first().subscribe(state => {
          this.router.navigate(['../forum/subject/' + this.post.info.subjectUid + '/post/' + post.uid]);
            this.uploading = false;
        },
        error => {
          this.uploading = false;
        });
      }, error => {
        this.uploading = false;
      });
    });
  }
}
