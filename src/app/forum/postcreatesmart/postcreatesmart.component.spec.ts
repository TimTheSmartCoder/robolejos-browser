import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostpagesmartComponent } from './postcreatesmart.component';

describe('PostpagesmartComponent', () => {
  let component: PostpagesmartComponent;
  let fixture: ComponentFixture<PostpagesmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostpagesmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostpagesmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
