import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Location} from '@angular/common';
import {PostService} from '../../../shared/services/post.service';
import {Subscription} from 'rxjs/Subscription';
import {Info, Post} from '../../../shared/models/post';





@Component({
  selector: 'app-postpage',
  templateUrl: './postcreate.component.html',
  styleUrls: ['./postcreate.component.css']
})
export class PostpageComponent implements OnInit {


  info: Info;
  images: any;

  @Input()
  public uploading = false;

  @Output()
    public createpost: EventEmitter<any>;


  constructor(private location: Location) {
    this.info = new Info();
    this.createpost = new EventEmitter<any>();
    this.images = [];
  }

  ngOnInit() {

  }

  cancel(): void {
    this.location.back();
  }

  onSubmit(e): void {
    //Prevents the default action on submit
    e.preventDefault();
    //Emits the event with information to the smart component
    this.createpost.emit({info: this.info, images: this.images});
  }

  getImages(images: any){
    this.images = images;
  }

}
