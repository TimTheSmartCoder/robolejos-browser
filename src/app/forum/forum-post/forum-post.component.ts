import {Component, OnInit, Input} from '@angular/core';
import {Post} from "../../shared/models/post";

@Component({
  selector: 'app-forum-post',
  templateUrl: 'forum-post.component.html',
  styleUrls: ['forum-post.component.css']
})
export class ForumPostComponent implements OnInit {

  @Input()
    post: Post;

  constructor() {

  }

  ngOnInit() {
  }

}
