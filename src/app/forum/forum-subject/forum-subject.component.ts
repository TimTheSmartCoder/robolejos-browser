import {Component, OnInit, Input} from '@angular/core';
import {Subject} from "../../shared/models/subject";
import {PostService} from "../../shared/services/post.service";
import {Post} from "../../shared/models/post";

@Component({
  selector: 'app-forum-subject',
  templateUrl: 'forum-subject.component.html',
  styleUrls: ['forum-subject.component.css']
})
export class ForumSubjectComponent implements OnInit {

  //The subject to display in the component
  @Input()
    subject: Subject;

  latestPost: Post;
  latestPostTitle: string;

  constructor(private service : PostService) {

  }

  ngOnInit() {
    //Gets the newest post from the subject
    this.service.getNewestPostFromSubject(this.subject.uid, 1).first().subscribe(list => {
      //Sets the latest post
      this.latestPost = list[0];
      if(this.latestPost != null) {
        //Gets the title of the latest post and shortens it if necessary
        this.latestPostTitle =
          this.latestPost.info.title.length < 15 ? this.latestPost.info.title :
            this.latestPost.info.title.substr(0, 15);
      }
    });
  }

}
