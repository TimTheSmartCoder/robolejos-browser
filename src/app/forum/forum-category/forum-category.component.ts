import {Component, OnInit, Input} from '@angular/core';
import {Category} from "../../shared/models/category";
import {Subject} from "../../shared/models/subject";

@Component({
  selector: 'app-forum-category',
  templateUrl: 'forum-category.component.html',
  styleUrls: ['forum-category.component.css']
})
export class ForumCategoryComponent implements OnInit {

  @Input()
    category: Category;

  constructor() {

  }

  ngOnInit() {
  }

}
