import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Post} from "../../shared/models/post";
import {PostService} from "app/shared/services/post.service";
import {User} from "../../shared/models/user";
import {UserService} from "../../shared/services/user.service";
import {unescapeIdentifier} from "@angular/compiler";

@Component({
  selector: 'app-forum-post-page',
  templateUrl: './forum-post-page.component.html',
  styleUrls: ['./forum-post-page.component.css']
})
export class ForumPostPageComponent implements OnInit, OnDestroy {

  post: Post;
  user: User;



  id: string;
  private sub: any;

  constructor(private route: ActivatedRoute,
              private postService: PostService,
              private userService: UserService
  ) {

  }

  ngOnInit() {

    //Gets the postid from the URL by getting the params defined in the routing module
    this.sub = this.route.params .subscribe(params => {
      this.id = params['postid'];
      console.log("post id", this.id);
      //Gets post by id
      this.postService.Get(this.id).first().subscribe(post => {
        console.log("ny", post);
        this.post = post;
        console.log(this.post);
        //Gets the user that is tied to the post
        this.userService.get(this.post.info.userUid).first().subscribe(user => {
          this.user = user;
        });
      });
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
