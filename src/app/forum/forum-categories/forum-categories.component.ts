import {Component, OnInit, Output, Input} from '@angular/core';
import { Category } from '../../shared/models/category';
import {Router} from "@angular/router";
import {Subject} from "../../shared/models/subject";
import {CategoryService} from "../../shared/services/category.service";
import {SubjectService} from "../../shared/services/subject.service";
import {forEach} from "@angular/router/src/utils/collection";



@Component({
  selector: 'app-forum-categories',
  templateUrl: './forum-categories.component.html',
  styleUrls: ['./forum-categories.component.css']
})
export class ForumCategoriesComponent implements OnInit {

  categories: Category[] = [];

  constructor(private categoryService : CategoryService, private subjectService : SubjectService) {

  }

  ngOnInit() {
    //Gets all categories
    this.categoryService.GetAll().first().subscribe(list => {
      this.categories = list;
      //Gets all subjects for each category and add them to their category
      for(let category of this.categories) {
        this.subjectService.GetByCategory(category.uid).first().subscribe(list => {
          category.subjects = list;
        });
      }
    });



  }
}
